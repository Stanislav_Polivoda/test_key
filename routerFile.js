const express = require("express");
const router = express.Router();
const validateFile = require("./validator");
const { createFile, getFiles, getFile } = require("./controlers");




//routr settings
router.route("/").post(validateFile, createFile);
router.route("/:filename").get(getFile);
router.route("/").get(getFiles);


module.exports = router;
