const path = require("path");

const validateFile = (req, res, next)=>{
  const fileExtensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
  const { filename, content } = req.body;

  if (!filename) {
    return res.status(400).json({ message: "error" })
  };

  if (fs.existsSync(`./files/${filename}`)) {
    return res.status(400).json({ message: "error" })
  };
  
  if (!fileExtensions.includes(path.extname(filename))) {
      return res.status(400).json({ message: "error" });
  };

  if (!content) {
      return res.status(400).json({ message: "error" });
  };

  next();

}
