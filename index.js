const express = require("express");
const app = express();
const routerFile = require("./routerFile");
const morgan = require("morgan");
//const serverPort = 8080;

app.use(express.json());
app.use(morgan("tiny"));

app.use("/files", routerFile);

app.listen(8080, ()=> {
  console.log("Server is working");
})
